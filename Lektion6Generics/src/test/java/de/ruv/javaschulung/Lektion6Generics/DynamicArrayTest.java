package de.ruv.javaschulung.Lektion6Generics;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class DynamicArrayTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public DynamicArrayTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(DynamicArrayTest.class);
	}

	public void testConstruct() {
		DynamicArray<Object> dArray1 = new DynamicArray<Object>();
		Class<? extends DynamicArray> arrayClass = dArray1.getClass();

		try {
			Field field = arrayClass.getDeclaredField("array");
			assertNotNull(field);

			assertEquals("Datentyp", Object[].class, field.getType());

			assertTrue("Feld ist nicht private", Modifier.isPrivate(field.getModifiers()));

			field.setAccessible(true);

			Object[] os = (Object[]) field.get(dArray1);

			assertNotNull(os);

			assertEquals(10, os.length);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld array fehlt oder wurde falsch implementiert!");
		}

		DynamicArray<String> dArray2 = new DynamicArray<String>(25);
		Class<? extends DynamicArray> arrayClass2 = dArray2.getClass();

		try {
			Field field = arrayClass2.getDeclaredField("array");
			assertNotNull(field);

			assertEquals("Datentyp", Object[].class, field.getType());

			assertTrue("Feld ist nicht private", Modifier.isPrivate(field.getModifiers()));

			field.setAccessible(true);

			Object[] os = (Object[]) field.get(dArray2);

			assertNotNull(os);

			assertEquals(25, os.length);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld array fehlt oder wurde falsch implementiert!");
		}
	}

	public void testAddNGet() {
		DynamicArray<String> dArray = new DynamicArray<>();

		for (int i = 0; i < 10; i++) {
			dArray.add(Integer.toString(i));
		}

		assertEquals(10, dArray.getLength());

		for (int i = 0; i < 10; i++) {
			assertEquals(Integer.toString(i), dArray.get(i));
		}
	}

	public void testAddAllocation() {
		DynamicArray<String> dArray = new DynamicArray<>();
		Class<? extends DynamicArray> arrayClass = dArray.getClass();

		for (int i = 0; i < 10; i++) {
			dArray.add(Integer.toString(i));
		}

		assertEquals(10, dArray.getLength());

		Object[] oldArray = null;
		try {
			Field field = arrayClass.getDeclaredField("array");

			field.setAccessible(true);

			oldArray = (Object[]) field.get(dArray);

			assertEquals(10, oldArray.length);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld array fehlt oder wurde falsch implementiert!");
		}

		dArray.add("10");

		assertEquals(11, dArray.getLength());

		Object[] newArray = null;
		try {
			Field field = arrayClass.getDeclaredField("array");

			field.setAccessible(true);

			newArray = (Object[]) field.get(dArray);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld array fehlt oder wurde falsch implementiert!");
		}
		assertEquals(20, newArray.length);

		assertNotSame(oldArray, newArray);

		for (int i = 11; i < 30; i++) {
			dArray.add(Integer.toString(i));
		}

		try {
			Field field = arrayClass.getDeclaredField("array");

			field.setAccessible(true);

			newArray = (Object[]) field.get(dArray);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld array fehlt oder wurde falsch implementiert!");
		}
		assertEquals(30, newArray.length);

		for (int i = 0; i < 30; i++) {
			assertEquals(Integer.toString(i), dArray.get(i));
		}
	}
	
	public void testAddRemove() {
		DynamicArray<String> dArray = new DynamicArray<>();
		Class<? extends DynamicArray> arrayClass = dArray.getClass();

		for (int i = 0; i < 42; i++) {
			dArray.add(Integer.toString(i));
		}

		assertEquals(42, dArray.getLength());

		Object[] oldArray = null;
		try {
			Field field = arrayClass.getDeclaredField("array");

			field.setAccessible(true);

			oldArray = (Object[]) field.get(dArray);

			assertEquals(50, oldArray.length);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld array fehlt oder wurde falsch implementiert!");
		}
		
		assertEquals(Integer.toString(21), dArray.get(21));

		assertNotNull(oldArray[41]);
		dArray.remove(21);
		assertNull(oldArray[41]);
		
		assertEquals(Integer.toString(22), dArray.get(21));
		
		
		assertEquals(41, dArray.getLength());
		
		assertEquals(Integer.toString(2), dArray.get(2));
		
		assertNotNull(oldArray[40]);
		dArray.remove(2);
		assertNull(oldArray[40]);
		
		assertEquals(Integer.toString(3), dArray.get(2));
		
		
		assertEquals(40, dArray.getLength());
	}
}
