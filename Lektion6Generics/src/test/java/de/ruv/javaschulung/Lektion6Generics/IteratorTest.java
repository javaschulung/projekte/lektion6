package de.ruv.javaschulung.Lektion6Generics;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class IteratorTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public IteratorTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(IteratorTest.class);
	}

	public void testImplements() {
		DynamicArray<Object> dArray = new DynamicArray<Object>();
		Class<? extends DynamicArray> arrayClass = dArray.getClass();

		Class<?>[] interfaces = arrayClass.getInterfaces();
		
		HashMap<String, Boolean> found = new HashMap<>();
		found.put("java.util.Iterator", false);
		found.put("java.lang.Iterable", false);
		
		for (Class<?> inface : interfaces) {
			for (String s : found.keySet()) {
				if (inface.getCanonicalName().equals(s)) {
					found.replace(s, true);
				}
			}
		}
		
		found.forEach((s, b) -> assertTrue("Klasse implementiert " + s + " nicht.", b));
	}
	
	public void testIterator() {
		DynamicArray<String> dArray = new DynamicArray<String>();
		
		for (int i = 0; i < 35; i++) {
			dArray.add(Integer.toString(i));
		}
		
		
		int i = 0;
		for (String s : dArray) {
			assertEquals(Integer.toString(i++), s);
		}
	}

}
